unit uItensPedidoControllers;

interface
  uses IItensPedido, uItensPedido, Data.DB, Data.Win.ADODB, Vcl.Grids, System.SysUtils;
  Type
      TItensPedidoController = class(TInterfacedObject, TIItensPedido)


      public
          procedure addItemPedido(pedido:integer;grid:TStringGrid; conn: TADOConnection; qry: TADOQuery; cmd: TADOCommand);
          procedure removeItemPedido(id: Integer; conn: TADOConnection; qry: TADOQuery; cmd: TADOCommand);
          function findItem(id: Integer; conn: TADOConnection; qry: TADOQuery; cmd: TADOCommand): TItensPedido;
  end;

implementation

  procedure TItensPedidoController.addItemPedido(pedido:integer; grid:TStringGrid; conn: TADOConnection; qry: TADOQuery; cmd: TADOCommand);
  begin
        for var I := 1 to grid.RowCount-1 do
          begin
              if grid.Cells[5,i] <> '' then
              begin
                var query := 'insert into itens_pedido(pedidoID, produtoID, qtde, vlunitario, vltotal) values(:cod, :prod, :qtde, :unit, :total)';
                cmd.CommandText := query;
                cmd.Parameters.ParamByName('cod').Value := pedido;
                cmd.Parameters.ParamByName('prod').Value := grid.Cells[1,i];
                cmd.Parameters.ParamByName('qtde').Value := grid.Cells[3,i];
                cmd.Parameters.ParamByName('unit').Value := grid.Cells[4,i];
                cmd.Parameters.ParamByName('total').Value := StrToFloat(grid.Cells[4,i]) * StrToInt(grid.Cells[3,i]);
                cmd.Execute;
              end;
          end;
  end;

  procedure TItensPedidoController.removeItemPedido(id: Integer; conn: TADOConnection; qry: TADOQuery; cmd: TADOCommand);
  begin

  end;

  function TItensPedidoController.findItem(id: Integer; conn: TADOConnection; qry: TADOQuery; cmd: TADOCommand): TItensPedido;
  begin

  end;
end.
