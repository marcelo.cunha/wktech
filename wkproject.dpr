program wkproject;

uses
  Vcl.Forms,
  main in 'main.pas' {Form1},
  uPeddo in 'uPeddo.pas',
  IPedido in 'IPedido.pas',
  uPedidoController in 'uPedidoController.pas',
  uItensPedido in 'uItensPedido.pas',
  IItensPedido in 'IItensPedido.pas',
  uItensPedidoControllers in 'uItensPedidoControllers.pas',
  uProduto in 'uProduto.pas',
  IProduto in 'IProduto.pas',
  uProdutoController in 'uProdutoController.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
