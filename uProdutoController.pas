unit uProdutoController;

interface
  uses IProduto, uItensPedido, Data.DB, Data.Win.ADODB, Vcl.Grids, System.SysUtils;
  Type
      TProdutoController = class(TInterfacedObject, TIProduto)
            public
                function findById(id: Integer; conn: TADOConnection; qry: TADOQuery): string;

      end;

implementation

     function TProdutoController.findById(id: Integer; conn: TADOConnection; qry: TADOQuery): string;
     begin
       qry.Close;
       qry.SQL.Clear;
       qry.SQL.Add('Select descricao from whdb.produtos where codigo = :codigo');
       qry.Parameters.ParamByName('codigo').value := id;
       qry.open;
       result := qry.FieldByName('descricao').AsString;
     end;

end.
