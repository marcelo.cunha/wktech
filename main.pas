unit main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  Vcl.ComCtrls, Vcl.Grids, Data.DB, Data.Win.ADODB, uPedidoController,uProdutoController;

type
  TForm1 = class(TForm)
    StatusBar1: TStatusBar;
    edtCodProd: TEdit;
    edtVlUnit: TEdit;
    edtQtde: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Button1: TButton;
    StringGrid1: TStringGrid;
    Button2: TButton;
    edtCodCli: TEdit;
    Label4: TLabel;
    btnListar: TButton;
    btnCancelar: TButton;
    ADOConnection1: TADOConnection;
    ADOQuery1: TADOQuery;
    ADOCommand1: TADOCommand;
    procedure FormCreate(Sender: TObject);
    procedure atualizarTotal(Sender: TObject);
    procedure LimparCampos(Sender: TObject);
    procedure LimparTudo(Sender: TObject);
    procedure AlterarDados(Sender: TObject; linha : integer);
    procedure Button1Click(Sender: TObject);
    procedure StringGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Button2Click(Sender: TObject);
    procedure edtCodCliChange(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnListarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.btnCancelarClick(Sender: TObject);
begin
    var numeropedido: string;
    numeropedido := InputBox('Cancelamento de Pedido','Informe o numero do Pedido', '');
    var PediControl := TPedidoController.Create;
    PediControl.removePedido(strtoint(numeropedido),ADOConnection1,ADOQuery1,ADOCommand1);
end;

procedure TForm1.btnListarClick(Sender: TObject);
begin
  var numeropedido: string;
  numeropedido := InputBox('Listar Pedido','Informe o numero do Pedido', '');
  var ped := TPedidoController.Create;
  var resultado := TADOQuery.Create(self);
  var adoaux := TADOQuery.Create(self);
  adoaux.Connection := ADOConnection1;
  resultado := ped.findPedido( strToint(numeropedido),ADOConnection1,ADOQuery1);
  var i : integer;
  i:= 1;
  var prod := TProdutoController.Create;
  while(not resultado.Eof) do
  begin
    edtCodCli.Text := resultado.Recordset.Fields['ClienteID'].Value;;
    StringGrid1.Cells[1,i] := resultado.Recordset.Fields['produtoID'].Value;
    StringGrid1.Cells[3,i] := resultado.Recordset.Fields['qtde'].Value;
    StringGrid1.Cells[4,i] := resultado.Recordset.Fields['vlunitario'].Value;
    StringGrid1.Cells[5,i] := resultado.Recordset.Fields['vltotal'].Value;
    StringGrid1.Cells[2,i] := prod.findById(strToint(StringGrid1.Cells[1,i]), ADOConnection1, adoaux);


    StringGrid1.RowCount := StringGrid1.RowCount+ 1;
    i := i + 1;


    resultado.Next;
  end;

end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  var prod := TProdutoController.Create;
  StringGrid1.Cells[1,StringGrid1.RowCount-1] := edtCodProd.Text;
  StringGrid1.Cells[2,StringGrid1.RowCount-1] := prod.findById(strToint(edtCodProd.Text), ADOConnection1, ADOQuery1);
  StringGrid1.Cells[3,StringGrid1.RowCount-1] := edtQtde.Text;
  StringGrid1.Cells[4,StringGrid1.RowCount-1] := edtVlUnit.Text;
  StringGrid1.Cells[5,StringGrid1.RowCount-1] := FloatToStr(StrToInt(edtQtde.Text)* StrToFloat(edtVlUnit.Text));
  StringGrid1.RowCount :=  StringGrid1.RowCount+1;
  atualizarTotal(self);
  LimparCampos(self);
  edtCodProd.ReadOnly := false;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  var PediControl := TPedidoController.Create;
  PediControl.addPedido(Date,strtoint(edtCodCli.Text),StrToFloat(StatusBar1.Panels.Items[1].Text),ADOConnection1,ADOQuery1,ADOCommand1,StringGrid1);
  LimparTudo(self);
end;

procedure TForm1.edtCodCliChange(Sender: TObject);
begin
    if (edtCodCli.Text = '') then
    begin
        btnListar.Visible := true;
        btnCancelar.Visible := true;
    end
    else begin
        btnListar.Visible := false;
        btnCancelar.Visible := false;
    end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  StringGrid1.Cells[1,0] := 'Cod. Produto';
  StringGrid1.Cells[2,0] := 'Descri��o';
  StringGrid1.Cells[3,0] := 'Quantidade';
  StringGrid1.Cells[4,0] := 'Vl. Unit';
  StringGrid1.Cells[5,0] := 'Vl. Total';
end;

procedure TForm1.StringGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if VK_RETURN = Key then
  begin
    AlterarDados(self, StringGrid1.Row);
    key := 46;
  end;

  if VK_DELETE = Key  then
    begin
      if(StringGrid1.Row = StringGrid1.RowCount - 1 ) then
        StringGrid1.RowCount :=  StringGrid1.RowCount - 1;
      for var i := StringGrid1.Row to StringGrid1.RowCount - 1 do
      begin
           StringGrid1.Rows[i] := StringGrid1.Rows[i+1]
      end;
      StringGrid1.RowCount :=  StringGrid1.RowCount - 1;
      atualizarTotal(self);
    end;
end;

procedure TForm1.atualizarTotal(Sender: TObject);
begin
  var total : double;
  total := 0;
  for var i := 1 to StringGrid1.RowCount - 1 do
      begin
          if StringGrid1.Cells[5,i] <> '' then
            total :=  total + StrToFloat(StringGrid1.Cells[5,i]);
      end;
      StatusBar1.Panels.Items[1].Text := FloatToStr(total);
end;

procedure TForm1.LimparCampos(Sender: TObject);
begin
  edtCodProd.Clear;
  edtQtde.Clear;
  edtVlUnit.Clear
end;

procedure TForm1.LimparTudo(Sender: TObject);
begin

  LimparCampos(self);
  for var i := 1 to StringGrid1.RowCount-1 do
  begin
    StringGrid1.Rows[i] :=  StringGrid1.Rows[i+1];
    StringGrid1.Cells[1,i] := '';
    StringGrid1.Cells[2,i] := '';
    StringGrid1.Cells[3,i] := '';
    StringGrid1.Cells[4,i] := '';
    StringGrid1.Cells[5,i] := '';
    StringGrid1.RowCount := StringGrid1.RowCount-1;
  end;
  StringGrid1.RowCount :=  StringGrid1.RowCount+1;

end;

//procedure TForm1.LimparGrid(Sender: TObject);
//begin
//  LimparCampos(self);
//  for var i := 1 to StringGrid1.RowCount-1 do
//      StringGrid1.Rows[i] :=  StringGrid1.Rows[i+1];
//end;

procedure TForm1.AlterarDados(Sender: TObject; linha:integer);
begin
    edtQtde.Text := StringGrid1.Cells[3,linha];
    edtVlUnit.Text := StringGrid1.Cells[4,linha];
    edtCodProd.Text := StringGrid1.Cells[1,linha];
    edtCodProd.ReadOnly := true;
end;


end.
