unit IPedido;

interface

  uses uPeddo,Data.DB, Data.Win.ADODB, Vcl.Grids;

  Type
      TIPedido = Interface
         procedure addPedido(dtemissao:TDateTime; clienteID : integer; total: float64; conn : TAdoConnection; qry:TAdoQuery; cmd: TAdoCommand; grid:TStringGrid );
         procedure removePedido(id : integer; conn : TAdoConnection; qry:TAdoQuery; cmd: TAdoCommand);
         function findPedido(id: integer; conn : TAdoConnection; qry:TAdoQuery) : TAdoQuery;
      End;

implementation

end.
