object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Vendas'
  ClientHeight = 475
  ClientWidth = 753
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 53
    Width = 33
    Height = 13
    Caption = 'Codigo'
  end
  object Label2: TLabel
    Left = 200
    Top = 53
    Width = 56
    Height = 13
    Caption = 'Quantidade'
  end
  object Label3: TLabel
    Left = 344
    Top = 53
    Width = 64
    Height = 13
    Caption = 'Valor Unit'#225'rio'
  end
  object Label4: TLabel
    Left = 8
    Top = 7
    Width = 69
    Height = 13
    Caption = 'Codigo Cliente'
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 456
    Width = 753
    Height = 19
    Panels = <
      item
        Alignment = taRightJustify
        Text = 'Total : R$ '
        Width = 250
      end
      item
        Width = 250
      end
      item
        Alignment = taRightJustify
        Width = 50
      end>
  end
  object edtCodProd: TEdit
    Left = 8
    Top = 72
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object edtVlUnit: TEdit
    Left = 344
    Top = 72
    Width = 121
    Height = 21
    TabOrder = 2
  end
  object edtQtde: TEdit
    Left = 200
    Top = 72
    Width = 121
    Height = 21
    TabOrder = 3
  end
  object Button1: TButton
    Left = 543
    Top = 70
    Width = 75
    Height = 25
    Caption = '&Incluir'
    TabOrder = 4
    OnClick = Button1Click
  end
  object StringGrid1: TStringGrid
    Left = 8
    Top = 112
    Width = 753
    Height = 338
    Align = alCustom
    ColCount = 6
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goRowSelect, goFixedRowDefAlign]
    TabOrder = 5
    OnKeyDown = StringGrid1KeyDown
    ColWidths = (
      64
      76
      280
      98
      91
      96)
  end
  object Button2: TButton
    Left = 624
    Top = 70
    Width = 97
    Height = 25
    Caption = 'Gravar Pedido'
    TabOrder = 6
    OnClick = Button2Click
  end
  object edtCodCli: TEdit
    Left = 8
    Top = 26
    Width = 121
    Height = 21
    TabOrder = 7
    OnChange = edtCodCliChange
  end
  object btnListar: TButton
    Left = 200
    Top = 22
    Width = 96
    Height = 25
    Caption = 'Listar Pedido'
    TabOrder = 8
    OnClick = btnListarClick
  end
  object btnCancelar: TButton
    Left = 344
    Top = 22
    Width = 98
    Height = 25
    Caption = 'Cancelar Pedido'
    TabOrder = 9
    OnClick = btnCancelarClick
  end
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=MSDASQL.1;Password=root;Persist Security Info=True;User' +
      ' ID=root;Data Source=mysql;Initial Catalog=whdb'
    LoginPrompt = False
    Left = 528
    Top = 16
  end
  object ADOQuery1: TADOQuery
    Connection = ADOConnection1
    Parameters = <>
    Left = 616
    Top = 16
  end
  object ADOCommand1: TADOCommand
    Connection = ADOConnection1
    Parameters = <>
    Left = 448
    Top = 16
  end
end
