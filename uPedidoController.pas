unit uPedidoController;

interface

    uses IPedido,uItensPedido, uItensPedidoControllers, uPeddo, Data.DB, Data.Win.ADODB, System.SysUtils, Vcl.Grids,Vcl.Dialogs;

    Type
        TPedidoController = class(TInterfacedObject, TIPedido)


        public
          procedure addPedido(dtemissao: TDateTime; clienteID: Integer; total: Double; conn : TAdoConnection; qry:TAdoQuery; cmd: TAdoCommand; grid:TStringGrid);
          procedure removePedido(id: Integer; conn : TAdoConnection; qry:TAdoQuery; cmd: TAdoCommand);
          function findPedido(id: Integer; conn : TAdoConnection; qry:TAdoQuery): TAdoQuery;
          function ProximoPedido(conn:TAdoConnection; qry: TAdoQuery):integer ;


    End;

implementation

  procedure TPedidoController.addPedido(dtemissao: TDateTime; clienteID: Integer; total: Double; conn : TAdoConnection; qry:TAdoQuery; cmd: TAdoCommand; grid:TStringGrid);
  Begin
    var query : string;
    try
        conn.BeginTrans;
        var lastOrder := ProximoPedido(conn,qry);
        if lastOrder < 0  then
            lastOrder := 0;
        query := 'insert into pedido(numeropedido, dtemissao, clienteID, vltotal) values(:cod, :data, :clienteid, :total)';
        cmd.CommandText := query;
        cmd.Parameters.ParamByName('cod').Value := lastOrder + 1;
        cmd.Parameters.ParamByName('data').Value :=  FormatDateTime('YYYY/MM/DD',dtemissao);
        cmd.Parameters.ParamByName('clienteid').Value := clienteID;
        cmd.Parameters.ParamByName('total').Value := total;
        cmd.Execute;
        var item := TItensPedidoController.Create;
        item.addItemPedido(lastOrder + 1, grid, conn, qry, cmd );
        conn.CommitTrans;
    Except
        conn.RollbackTrans;
    end;

  End;
  procedure TPedidoController.removePedido(id: Integer; conn : TAdoConnection; qry:TAdoQuery; cmd: TAdoCommand);
  Begin

  try
    var query: string;
    conn.BeginTrans;
    query := 'DELETE FROM Itens_Pedido Where pedidoid = :codigo';
    cmd.CommandText := query;
    cmd.Parameters.ParamByName('codigo').Value := id;
    cmd.Execute;
    query := 'DELETE FROM Pedido Where numeropedido = :codigo';
    cmd.CommandText := query;
    cmd.Parameters.ParamByName('codigo').Value := id;
    cmd.Execute;
    conn.CommitTrans;
  except
      ShowMessage('Erro');
      conn.RollbackTrans;
  end;


  End;
  function TPedidoController.findPedido(id: Integer; conn : TAdoConnection; qry:TAdoQuery): TAdoquery;
  Begin
      qry.Close;
      qry.SQL.Clear;
      qry.SQL.Add('Select p.clienteId,i.produtoID, i.qtde, i.vlunitario, i.vltotal from whdb.Pedido p, whdb.itens_Pedido i  where p.numeropedido = i.pedidoId and  p.numeroPedido = :cod');
      qry.Parameters.ParamByName('cod').Value := id;
      qry.Open;
      result := qry;
  End;


  function TPedidoController.ProximoPedido(conn:TAdoConnection; qry: TAdoQuery):integer ;
  begin
    qry.SQL.Clear;
    qry.SQL.Add('Select max(numeropedido) as ultimo from whdb.pedido;');
    qry.open;
    result := qry.FieldByName('ultimo').AsInteger;

  end;
end.
