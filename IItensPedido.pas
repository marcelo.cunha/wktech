unit IItensPedido;

interface

  uses uItensPedido,Data.DB, Data.Win.ADODB, Vcl.Grids;

    Type
        TIItensPedido = Interface
         procedure addItemPedido(pedido:integer; grid:TStringGrid; conn : TAdoConnection; qry:TAdoQuery; cmd: TAdoCommand );
         procedure removeItemPedido(id : integer; conn : TAdoConnection; qry:TAdoQuery; cmd: TAdoCommand);
         function findItem(id: integer; conn : TAdoConnection; qry:TAdoQuery; cmd: TAdoCommand) : TItensPedido;
        End;

implementation

end.
